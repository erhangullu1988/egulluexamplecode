package com.erhangullu.usecase.validation;


import com.erhangullu.usecase.entity.MeterReading;
import com.erhangullu.usecase.exception.MeterDataNotExistsException;
import com.erhangullu.usecase.exception.NotFoundException;
import com.erhangullu.usecase.service.MeterReadingService;
import lombok.RequiredArgsConstructor;

import java.util.List;


@RequiredArgsConstructor
public class ConsumptionValidation {

    private final MeterReadingService meterReadingService;

    public void validateParameters(Integer connection) {
        validateConnectionExist(connection);
    }

    private void validateConnectionExist(Integer connection) {
        List<MeterReading> meterReadings = null;
        try {
            meterReadings = meterReadingService.findByConnection(connection);
        } catch (NotFoundException ex) {
            ex.printStackTrace();
        }

        if (meterReadings == null) {
            throw new MeterDataNotExistsException(connection);
        }
    }
}
