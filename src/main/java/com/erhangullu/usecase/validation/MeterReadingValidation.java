package com.erhangullu.usecase.validation;

import com.erhangullu.usecase.entity.Fraction;
import com.erhangullu.usecase.entity.MeterReading;
import com.erhangullu.usecase.exception.NotFoundException;
import com.erhangullu.usecase.service.FractionService;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.time.Month;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toMap;

@RequiredArgsConstructor
public class MeterReadingValidation {

    private final FractionService fractionService;

    public ValidationResult filterMeterReadings(List<MeterReading> meterReadingList) {
        ValidationResult filterResult = new ValidationResult(meterReadingList);
        filterMeterReadingIncreaseInConsecutiveMonths(filterResult);
        filterFractionDataExitsAndMeterValuesValid(filterResult);
        return filterResult;
    }


    private void filterFractionDataExitsAndMeterValuesValid(ValidationResult validationResult) {
        Map<Integer, List<MeterReading>> meterReadingMapByProfile = validationResult.getValidReadings()
                .stream()
                .collect(groupingBy(MeterReading::getConnection));

        Map<Integer, String> connectionProfileMap = validationResult.getValidReadings()
                .stream()
                .collect(toMap(MeterReading::getConnection, MeterReading::getProfile, (p1, p2) -> p1));

        meterReadingMapByProfile.forEach((connection, meterReadings) -> {
            meterReadings.sort(Comparator.comparing(MeterReading::getMonth));
            String profile = connectionProfileMap.get(connection);
            List<Fraction> fractionList = null;

            try {
                fractionList = fractionService.findByProfile(profile);
            } catch (NotFoundException ex) {
                ex.printStackTrace();
            }

            boolean invalid = false;

            if (fractionList == null) {
                invalid = true;
            } else {
                Map<Month, Double> monthFractionMap = fractionList.stream()
                        .collect(toMap(Fraction::getMonth, Fraction::getValue));

                Long totalConsumption = meterReadings.get(11).getValue();


                for (int i = 1; i < meterReadings.size(); i++) {
                    MeterReading previousMonthReading = meterReadings.get(i - 1);
                    MeterReading currentMonthReading = meterReadings.get(i);

                    Long currentMonthConsumption = currentMonthReading.getValue() - previousMonthReading.getValue();

                    Month currentMonth = currentMonthReading.getMonth();
                    Double currentFraction = monthFractionMap.get(currentMonth);
                    Double expectedConsumption = totalConsumption * currentFraction;
                    Double consumptionTolerance = expectedConsumption * 0.25d;


                    if (currentMonthConsumption < expectedConsumption - consumptionTolerance
                            || currentMonthConsumption > expectedConsumption + consumptionTolerance) {
                        invalid = true;
                    }
                }
            }

            if (invalid) {
                validationResult.getValidReadings().removeAll(meterReadings);
                validationResult.getInvalidConnections().add(connection);
            }
        });
    }

    private void filterMeterReadingIncreaseInConsecutiveMonths(ValidationResult validationResult) {
        Map<Integer, List<MeterReading>> meterReadingMapByProfile = validationResult.getValidReadings()
                .stream()
                .collect(groupingBy(MeterReading::getConnection));

        meterReadingMapByProfile.forEach((connection, meterReadings) -> {
            meterReadings.sort(Comparator.comparing(MeterReading::getMonth));

            boolean invalid = false;
            for (int i = 1; i < meterReadings.size(); i++) {
                Long currentMonth = meterReadings.get(i).getValue();
                Long previousMonth = meterReadings.get(i - 1).getValue();

                if (currentMonth < previousMonth) {
                    invalid = true;
                }
            }

            if (invalid) {
                validationResult.getValidReadings().removeAll(meterReadings);
                validationResult.getInvalidConnections().add(connection);
            }
        });
    }

    @Getter
    @Setter
    public class ValidationResult {
        List<MeterReading> validReadings;
        List<Integer> invalidConnections;

        public ValidationResult() {
            this.setInvalidConnections(new ArrayList<>());
            this.setValidReadings(new ArrayList<>());
        }

        public ValidationResult(List<MeterReading> meterReadings) {
            this();
            validReadings.addAll(meterReadings);
        }

        public boolean hasValidReadings() {
            return !validReadings.isEmpty();
        }

        public boolean hasInvalidReadings() {
            return !invalidConnections.isEmpty();
        }

    }
}
