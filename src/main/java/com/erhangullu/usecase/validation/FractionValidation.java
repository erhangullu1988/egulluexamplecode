package com.erhangullu.usecase.validation;

import com.erhangullu.usecase.entity.Fraction;
import com.erhangullu.usecase.exception.FractionDataAlreadyExistsException;
import com.erhangullu.usecase.exception.InvalidFractionExeption;
import com.erhangullu.usecase.exception.NotFoundException;
import com.erhangullu.usecase.service.FractionService;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class FractionValidation {

    private final FractionService fractionService;

    public void validateFractionList(List<Fraction> fractionList) {
        validateFractionSums(fractionList);
        validateProfileNotExists(fractionList);
    }

    private void validateFractionSums(List<Fraction> fractionList) {
        Map<String, Double> map = fractionList.stream().collect(Collectors.groupingBy(Fraction::getProfile, Collectors.summingDouble(Fraction::getValue)));

        List<String> invalidProfiles = new ArrayList<>();

        map.forEach((profile, fractionSum) -> {
            if (fractionSum != 1) {
                invalidProfiles.add(profile);
            }
        });

        if (!invalidProfiles.isEmpty()) {
            throw new InvalidFractionExeption(invalidProfiles);
        }
    }

    private void validateProfileNotExists(List<Fraction> fractionList) {
        List<String> profiles = fractionList.stream().map(Fraction::getProfile).distinct().collect(Collectors.toList());

        List<String> invalidProfiles = new ArrayList<>();

        profiles.forEach(profile -> {
            List profileFractions = null;
            try {
                profileFractions = fractionService.findByProfile(profile);
            } catch (NotFoundException ex) {
                ex.printStackTrace();
            }

            if (profileFractions != null) {
                invalidProfiles.add(profile);
            }
        });

        if (!invalidProfiles.isEmpty()) {
            throw new FractionDataAlreadyExistsException(invalidProfiles);
        }
    }
}
