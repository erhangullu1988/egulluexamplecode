package com.erhangullu.usecase.repository;


import com.erhangullu.usecase.entity.Fraction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;


public interface FractionRepository extends JpaRepository<Fraction, Long> {

    @Query("SELECT f FROM Fraction f WHERE LOWER(f.profile) = LOWER(:profile)")
    List<Fraction> findByProfile(@Param("profile") String profile);

    @Transactional
    @Modifying
    @Query("DELETE FROM Fraction f WHERE LOWER(f.profile) = LOWER(:profile)")
    Integer deleteByProfile(@Param("profile") String profile);
}


