package com.erhangullu.usecase.repository;

import com.erhangullu.usecase.entity.MeterReading;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;

public interface MeterReadingRepository extends JpaRepository<MeterReading, Long> {

    @Query("SELECT m FROM MeterReading m WHERE LOWER(m.month) = LOWER(:month) AND LOWER(m.connection) = LOWER(:connection)")
    MeterReading findByConnectionAndMonth(@Param("connection") Integer connection, @Param("month") String month);

    @Query("SELECT m FROM MeterReading m WHERE LOWER(m.connection) = LOWER(:connection)")
    List<MeterReading> findByConnection(@Param("connection") Integer connection);

    @Query("SELECT m FROM MeterReading m WHERE LOWER(m.month) = LOWER(:month)")
    List<MeterReading> findByMonth(@Param("month") String month);

    @Transactional
    @Modifying
    @Query("DELETE FROM MeterReading f WHERE LOWER(f.connection) = LOWER(:connection)")
    Integer deleteByConnection(@Param("connection") Integer connection);
}
