package com.erhangullu.usecase.service;

import com.erhangullu.usecase.entity.Consumption;

import java.time.Month;
import java.util.List;

public interface ConsumptionService {

    Consumption find(Month month, Integer connection);

    List<Consumption> findByConnection(Integer connection);
}
