package com.erhangullu.usecase.service;

import com.erhangullu.usecase.entity.Fraction;

import java.util.List;

public interface FractionService {

    void addAll(Iterable<Fraction> fractions);

    List<Fraction> findAll();

    List<Fraction> findByProfile(String profile);

    void deleteByProfile(String profile);
}
