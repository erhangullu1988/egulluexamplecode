package com.erhangullu.usecase.service;

import com.erhangullu.usecase.entity.MeterReading;
import com.erhangullu.usecase.exception.NotFoundException;
import com.erhangullu.usecase.repository.MeterReadingRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Month;
import java.util.List;

public class H2MeterReadingServiceImpl implements MeterReadingService {

    public static final String CONNECTION_ENTITY_TYPE = "Connection";
    @Autowired
    MeterReadingRepository meterReadingRepository;

    @Override
    public void addAll(List<MeterReading> objList) {
        meterReadingRepository.save(objList);
        meterReadingRepository.flush();
    }

    @Override
    public List<MeterReading> findByConnection(Integer connection) {
        List<MeterReading> meterReading = meterReadingRepository.findByConnection(connection);
        if (meterReading.isEmpty()) {
            throw new NotFoundException(CONNECTION_ENTITY_TYPE, connection.toString());
        }
        return meterReading;
    }

    @Override
    public MeterReading findByConnectionAndMonth(Integer connection, Month month) {
        MeterReading meterReading = meterReadingRepository.findByConnectionAndMonth(connection, month.toString());
        if (meterReading == null) {
            throw new NotFoundException(CONNECTION_ENTITY_TYPE, connection.toString());
        }
        return meterReading;
    }

    @Override
    public List<MeterReading> findAll() {
        return meterReadingRepository.findAll();
    }

    @Override
    public void deleteByConnection(Integer connection) {
        Integer result = meterReadingRepository.deleteByConnection(connection);
        if (result == 0) {
            throw new NotFoundException(CONNECTION_ENTITY_TYPE, connection.toString());
        }
    }

}
