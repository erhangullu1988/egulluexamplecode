package com.erhangullu.usecase.service;

import com.erhangullu.usecase.entity.Fraction;
import com.erhangullu.usecase.exception.NotFoundException;
import com.erhangullu.usecase.repository.FractionRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class H2FractionServiceImpl implements FractionService {

    @Autowired
    FractionRepository fractionRepository;

    @Override
    public void addAll(Iterable<Fraction> fractions) {
        fractionRepository.save(fractions);
        fractionRepository.flush();
    }

    @Override
    public List<Fraction> findAll() {
        return fractionRepository.findAll();
    }

    @Override
    public List<Fraction> findByProfile(String profile) {
        List<Fraction> fractionList = fractionRepository.findByProfile(profile);
        if (fractionList.isEmpty()) {
            throw new NotFoundException("Profile", profile);
        }

        return fractionList;
    }

    @Override
    public void deleteByProfile(String profile) {
        Integer result = fractionRepository.deleteByProfile(profile);
        if (result == 0) {
            throw new NotFoundException("Profile", profile);
        }
        fractionRepository.flush();
    }
}
