package com.erhangullu.usecase.service;

import com.erhangullu.usecase.entity.MeterReading;

import java.time.Month;
import java.util.List;

public interface MeterReadingService {

    void addAll(List<MeterReading> objList);

    List<MeterReading> findAll();

    List<MeterReading> findByConnection(Integer connection);

    MeterReading findByConnectionAndMonth(Integer connection, Month month);

    void deleteByConnection(Integer connection);


}
