package com.erhangullu.usecase.service;

import com.erhangullu.usecase.entity.Consumption;
import com.erhangullu.usecase.entity.MeterReading;
import com.erhangullu.usecase.exception.MeterDataNotExistsException;
import com.erhangullu.usecase.repository.MeterReadingRepository;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Month;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class H2ConsumptionServiceImpl implements ConsumptionService {

    @Autowired
    MeterReadingRepository meterReadingRepository;

    @Override
    public Consumption find(Month month, Integer connection) {
        MeterReading currentMonthReading = meterReadingRepository.findByConnectionAndMonth(connection, month.toString());
        if (currentMonthReading == null) {
            throw new MeterDataNotExistsException(connection);
        }

        if (month.equals(Month.JANUARY)) {
            return new Consumption(connection, month, currentMonthReading.getValue());
        }

        MeterReading previousMonthReading = meterReadingRepository.findByConnectionAndMonth(connection, month.minus(1).toString());
        Long consumption = currentMonthReading.getValue() - previousMonthReading.getValue();
        return new Consumption(connection, month, consumption);
    }

    @Override
    public List<Consumption> findByConnection(Integer connection) {
        List<MeterReading> meterReadings = meterReadingRepository.findByConnection(connection);
        Map<Month, MeterReading> meterReadingByMonth = meterReadings.stream().collect(Collectors.toMap(MeterReading::getMonth, meterReading -> meterReading));

        List<Consumption> consumptions = new ArrayList<>();

        Lists.newArrayList(Month.values()).forEach(month -> {
            MeterReading currentMonthReading = meterReadingByMonth.get(month);

            if (month.equals(Month.JANUARY)) {
                consumptions.add(new Consumption(connection, month, currentMonthReading.getValue()));
            } else {
                MeterReading previousMonthReading = meterReadingByMonth.get(month.minus(1));
                Long consumption = currentMonthReading.getValue() - previousMonthReading.getValue();
                consumptions.add(new Consumption(connection, month, consumption));
            }
        });

        return consumptions;
    }

}
