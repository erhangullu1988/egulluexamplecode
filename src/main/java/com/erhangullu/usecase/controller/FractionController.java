package com.erhangullu.usecase.controller;


import com.erhangullu.usecase.entity.Fraction;
import com.erhangullu.usecase.service.FractionService;
import com.erhangullu.usecase.validation.FractionValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/fraction")
public class FractionController {

    @Autowired
    FractionService fractionService;

    @Autowired
    FractionValidation fractionValidation;

    @RequestMapping(method = RequestMethod.PUT)
    @ResponseStatus(code = HttpStatus.CREATED)
    List<Fraction> addFractions(@RequestBody List<Fraction> fractionList) {
        fractionValidation.validateFractionList(fractionList);
        fractionService.addAll(fractionList);
        return fractionList;
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseStatus(code = HttpStatus.OK)
    List<Fraction> getAllFractions() {
        return fractionService.findAll();
    }

    @RequestMapping(path = "/{profile}", method = RequestMethod.GET)
    @ResponseStatus(code = HttpStatus.OK)
    List<Fraction> getProfileFractions(@PathVariable String profile) {
        return fractionService.findByProfile(profile);
    }

    @RequestMapping(path = "/{profile}", method = RequestMethod.DELETE)
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    void deleteProfileFractions(@PathVariable String profile) {
        fractionService.deleteByProfile(profile);
    }
}
