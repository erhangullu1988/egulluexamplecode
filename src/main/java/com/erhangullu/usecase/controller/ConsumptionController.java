package com.erhangullu.usecase.controller;

import com.erhangullu.usecase.entity.Consumption;
import com.erhangullu.usecase.service.ConsumptionService;
import com.erhangullu.usecase.validation.ConsumptionValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.time.Month;
import java.util.List;

@RestController
@RequestMapping(path = "/consumption")
public class ConsumptionController {

    @Autowired
    ConsumptionService consumptionService;

    @Autowired
    ConsumptionValidation consumptionValidation;


    @RequestMapping(path = "", method = RequestMethod.GET)
    public Consumption getConsumptionForProfileWithMonth(Month month, Integer connection) {
        consumptionValidation.validateParameters(connection);
        return consumptionService.find(month, connection);
    }

    @RequestMapping(path = "/connection/{connection}", method = RequestMethod.GET)
    public List<Consumption> getConsumptionsForProfile(@PathVariable("connection") Integer connection) {
        consumptionValidation.validateParameters(connection);
        return consumptionService.findByConnection(connection);
    }
}
