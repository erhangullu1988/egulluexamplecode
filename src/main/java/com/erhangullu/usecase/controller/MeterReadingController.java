package com.erhangullu.usecase.controller;


import com.erhangullu.usecase.entity.MeterReading;
import com.erhangullu.usecase.exception.InvalidReadingException;
import com.erhangullu.usecase.service.MeterReadingService;
import com.erhangullu.usecase.validation.MeterReadingValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.time.Month;
import java.util.List;

@RestController
@RequestMapping("/meterreading")
public class MeterReadingController {

    @Autowired
    MeterReadingService meterReadingService;

    @Autowired
    MeterReadingValidation meterReadingValidation;

    @RequestMapping(method = RequestMethod.PUT)
    @ResponseStatus(code = HttpStatus.CREATED)
    List<MeterReading> addMeterReadingData(@RequestBody List<MeterReading> meterReadings) {
        MeterReadingValidation.ValidationResult validationResult = meterReadingValidation.filterMeterReadings(meterReadings);
        if (validationResult.hasValidReadings()) {
            meterReadingService.addAll(validationResult.getValidReadings());
        }

        if (validationResult.hasInvalidReadings()) {
            throw new InvalidReadingException(validationResult);
        }

        return validationResult.getValidReadings();
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseStatus(code = HttpStatus.OK)
    List<MeterReading> getAllMeterReadings() {
        return meterReadingService.findAll();
    }

    @RequestMapping(path = "/{connection}", method = RequestMethod.GET)
    @ResponseStatus(code = HttpStatus.OK)
    List<MeterReading> getConnectionMeterReadings(@PathVariable Integer connection) {
        return meterReadingService.findByConnection(connection);
    }

    @RequestMapping(path = "/{connection}/{month}", method = RequestMethod.GET)
    @ResponseStatus(code = HttpStatus.OK)
    MeterReading getConnectionMeterReadingByMonth(@PathVariable Integer connection, @PathVariable Month month) {
        return meterReadingService.findByConnectionAndMonth(connection, month);
    }


    @RequestMapping(path = "/{connection}", method = RequestMethod.DELETE)
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    void deleteConnectionMeterReadings(@PathVariable Integer connection) {
        meterReadingService.deleteByConnection(connection);
    }
}
