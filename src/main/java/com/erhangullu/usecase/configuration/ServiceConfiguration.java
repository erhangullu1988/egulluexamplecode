package com.erhangullu.usecase.configuration;

import com.erhangullu.usecase.service.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ServiceConfiguration {

    @Bean
    public MeterReadingService meterReadingService() {
        return new H2MeterReadingServiceImpl();
    }

    @Bean
    public ConsumptionService consumptionService() {
        return new H2ConsumptionServiceImpl();
    }

    @Bean
    public FractionService fractionService() {
        return new H2FractionServiceImpl();
    }
}
