package com.erhangullu.usecase.configuration;

import com.erhangullu.usecase.service.FractionService;
import com.erhangullu.usecase.service.MeterReadingService;
import com.erhangullu.usecase.validation.ConsumptionValidation;
import com.erhangullu.usecase.validation.FractionValidation;
import com.erhangullu.usecase.validation.MeterReadingValidation;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ValidationConfiguration {

    @Bean
    FractionValidation fractionValidation(FractionService fractionService) {
        return new FractionValidation(fractionService);
    }

    @Bean
    ConsumptionValidation consumptionValidation(MeterReadingService meterReadingService) {
        return new ConsumptionValidation(meterReadingService);
    }

    @Bean
    MeterReadingValidation meterReadingValidation(FractionService fractionService) {
        return new MeterReadingValidation(fractionService);
    }
}
