package com.erhangullu.usecase.advice;

import com.erhangullu.usecase.exception.*;
import com.erhangullu.usecase.validation.MeterReadingValidation;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.stream.Collectors;


@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {


    @ExceptionHandler(value = {InvalidFractionExeption.class})
    protected ResponseEntity<Object> handleInvalidFractionExeption(InvalidFractionExeption ex, WebRequest request) {
        String bodyOfResponse = "Fraction data is invalid for following profiles: " + String.join(",", ex.getInvalidProfiles());
        return handleExceptionInternal(ex, new Error(bodyOfResponse),
                new HttpHeaders(), HttpStatus.NOT_ACCEPTABLE, request);
    }

    @ExceptionHandler(value = {FractionDataAlreadyExistsException.class})
    protected ResponseEntity<Object> handleFractionDataAlreadyExistsException(FractionDataAlreadyExistsException ex, WebRequest request) {
        String bodyOfResponse = "Fraction data already exists for profiles: " + String.join(",", ex.getInvalidFractions());
        return handleExceptionInternal(ex, new Error(bodyOfResponse),
                new HttpHeaders(), HttpStatus.NOT_ACCEPTABLE, request);
    }


    @ExceptionHandler(value = {MeterDataNotExistsException.class})
    protected ResponseEntity<Object> handleMeterDataNotExistsException(MeterDataNotExistsException ex, WebRequest request) {
        String bodyOfResponse = "Meter data does not exists for following connection: " + ex.getConnection();
        return handleExceptionInternal(ex, new Error(bodyOfResponse),
                new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }

    @ExceptionHandler(value = {NotFoundException.class})
    protected ResponseEntity<Object> handleNotFoundException(NotFoundException ex, WebRequest request) {
        String bodyOfResponse = "Connection not found: " + ex.getProfileName();
        return handleExceptionInternal(ex, new Error(bodyOfResponse),
                new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }

    @ExceptionHandler(value = {InvalidReadingException.class})
    protected ResponseEntity<Object> handleInvalidReadingException(InvalidReadingException ex, WebRequest request) {

        MeterReadingValidation.ValidationResult validationResult = ex.getValidationResult();
        HttpStatus status = HttpStatus.NOT_ACCEPTABLE;
        if (validationResult.hasValidReadings()) {
            status = HttpStatus.MULTI_STATUS;
        }

        String connectionIds = ex.getValidationResult().getInvalidConnections()
                .stream()
                .map(String::valueOf)
                .collect(Collectors.joining(","));

        String bodyOfResponse = "Invalid meter data for following connections: " + connectionIds;

        return handleExceptionInternal(ex, new Error(bodyOfResponse),
                new HttpHeaders(), status, request);
    }

}

