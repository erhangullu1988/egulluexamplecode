package com.erhangullu.usecase.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.Month;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "meterReading")
public class MeterReading {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @Column
    Integer connection;
    @Column
    String profile;
    @Column
    @Enumerated(EnumType.STRING)
    Month month;
    @Column
    Long value;


}
