package com.erhangullu.usecase.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.Month;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Consumption {
    private Integer connection;
    private Month month;
    private Long value;
}
