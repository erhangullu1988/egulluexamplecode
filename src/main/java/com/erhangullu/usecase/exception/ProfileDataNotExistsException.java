package com.erhangullu.usecase.exception;


import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class ProfileDataNotExistsException extends RuntimeException {
    private final String profile;
}
