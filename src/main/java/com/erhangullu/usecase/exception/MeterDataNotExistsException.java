package com.erhangullu.usecase.exception;


import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class MeterDataNotExistsException extends RuntimeException {
    private final Integer connection;
}
