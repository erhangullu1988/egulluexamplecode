package com.erhangullu.usecase.exception;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
@Getter
public class InvalidFractionExeption extends RuntimeException {
    private final List<String> invalidProfiles;
}
