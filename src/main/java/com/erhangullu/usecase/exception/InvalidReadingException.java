package com.erhangullu.usecase.exception;


import com.erhangullu.usecase.validation.MeterReadingValidation;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class InvalidReadingException extends RuntimeException {

    private final MeterReadingValidation.ValidationResult validationResult;
}
