package com.erhangullu.usecase.exception;


import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;

@Getter
@RequiredArgsConstructor
@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Fraction Already Exists")
public class FractionDataAlreadyExistsException extends RuntimeException {

    private final List<String> invalidFractions;
}
