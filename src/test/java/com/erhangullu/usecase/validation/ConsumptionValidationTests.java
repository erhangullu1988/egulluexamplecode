package com.erhangullu.usecase.validation;


import com.erhangullu.usecase.configuration.HelperConfiguration;
import com.erhangullu.usecase.configuration.ServiceConfiguration;
import com.erhangullu.usecase.configuration.ValidationConfiguration;
import com.erhangullu.usecase.entity.MeterReading;
import com.erhangullu.usecase.exception.MeterDataNotExistsException;
import com.erhangullu.usecase.helper.TestDataProvider;
import com.erhangullu.usecase.repository.FractionRepository;
import com.erhangullu.usecase.repository.MeterReadingRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {ServiceConfiguration.class, ValidationConfiguration.class, HelperConfiguration.class}, loader = AnnotationConfigContextLoader.class)
public class ConsumptionValidationTests {

    @Autowired
    TestDataProvider testDataProvider;

    @MockBean
    MeterReadingRepository meterReadingRepository;
    @MockBean
    FractionRepository fractionRepository;
    @Autowired
    ConsumptionValidation consumptionValidation;
    List<MeterReading> testConnectionReadings = new ArrayList<>();

    @Before
    public void prepareMockRepository() {

        testConnectionReadings = testDataProvider.getValidMeterReadingTestConnection1();

        when(meterReadingRepository.findByConnection(eq(TestDataProvider.TEST_CONNECTION_ID_1))).thenReturn(testConnectionReadings);
        when(meterReadingRepository.findByConnection(eq(TestDataProvider.INVALID_CONNECTION_1))).thenReturn(new ArrayList<MeterReading>());
    }

    @Test(expected = MeterDataNotExistsException.class)
    public void validateParametersShouldThrowExceptionWhenConnectionIsInvalid() {
        consumptionValidation.validateParameters(TestDataProvider.INVALID_CONNECTION_1);
    }

    @Test
    public void validateParametersShouldSuccessWhenConnectionIsValid() {
        consumptionValidation.validateParameters(TestDataProvider.TEST_CONNECTION_ID_1);
    }

}
