package com.erhangullu.usecase.validation;

import com.erhangullu.usecase.configuration.HelperConfiguration;
import com.erhangullu.usecase.configuration.ServiceConfiguration;
import com.erhangullu.usecase.configuration.ValidationConfiguration;
import com.erhangullu.usecase.entity.Fraction;
import com.erhangullu.usecase.entity.MeterReading;
import com.erhangullu.usecase.helper.TestDataProvider;
import com.erhangullu.usecase.repository.FractionRepository;
import com.erhangullu.usecase.repository.MeterReadingRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {ServiceConfiguration.class, ValidationConfiguration.class, HelperConfiguration.class}, loader = AnnotationConfigContextLoader.class)
public class MeterReadingValidationTests {

    @Autowired
    TestDataProvider testDataProvider;

    @MockBean
    MeterReadingRepository meterReadingRepository;
    @MockBean
    FractionRepository fractionRepository;
    @Autowired
    MeterReadingValidation fractionValidation;
    List<Fraction> testProfile1Fractions = new ArrayList<>();

    List<MeterReading> testProfile1Readings = new ArrayList<>();
    List<MeterReading> testProfileNonOrderedReadings = new ArrayList<>();
    List<MeterReading> testProfileNoFractionReadings = new ArrayList<>();

    @Before
    public void prepareMockRepository() {
        testProfile1Fractions = testDataProvider.getValidFractionTestProfile1();

        testProfile1Readings = testDataProvider.getValidMeterReadingTestConnection1();
        testProfileNonOrderedReadings = testDataProvider.getInvalidMeterReadingTestData();

        testProfileNoFractionReadings = testDataProvider.getValidMeterReadingTestConnection2();

        when(fractionRepository.findByProfile(eq(TestDataProvider.TEST_PROFILE_1))).thenReturn(testProfile1Fractions);
        when(fractionRepository.findByProfile(eq(TestDataProvider.INVALID_PROFILE_1))).thenReturn(testProfile1Fractions);
    }

    @Test
    public void filterMeterReadingsShouldNotHaveInvalidReadings() {
        MeterReadingValidation.ValidationResult validationResult = fractionValidation.filterMeterReadings(testProfile1Readings);

        assertThat(validationResult.hasValidReadings()).isTrue();
        assertThat(validationResult.hasInvalidReadings()).isFalse();
    }

    @Test
    public void filterMeterReadingsShouldHaveInvalidReadingsWhenValuesNotOrdered() {
        MeterReadingValidation.ValidationResult validationResult = fractionValidation.filterMeterReadings(testProfileNonOrderedReadings);

        assertThat(validationResult.hasValidReadings()).isFalse();
        assertThat(validationResult.hasInvalidReadings()).isTrue();
    }

    @Test
    public void filterMeterReadingsShouldHaveInvalidReadingsWhenFractionsNotExistsForProfile() {
        MeterReadingValidation.ValidationResult validationResult = fractionValidation.filterMeterReadings(testProfileNoFractionReadings);

        assertThat(validationResult.hasValidReadings()).isFalse();
        assertThat(validationResult.hasInvalidReadings()).isTrue();
    }

    @Test
    public void filterMeterReadingsShouldNotHaveValidAndInvalidReadings() {
        List<MeterReading> mixedReadings = new ArrayList<>();

        mixedReadings.addAll(testProfile1Readings);
        mixedReadings.addAll(testProfileNonOrderedReadings);
        mixedReadings.addAll(testProfileNoFractionReadings);

        MeterReadingValidation.ValidationResult validationResult = fractionValidation.filterMeterReadings(mixedReadings);

        assertThat(validationResult.hasValidReadings()).isTrue();
        assertThat(validationResult.hasInvalidReadings()).isTrue();

        assertThat(validationResult.getValidReadings()).size().isEqualTo(1 * 12);
        assertThat(validationResult.getInvalidConnections()).size().isEqualTo(2);
    }
}
