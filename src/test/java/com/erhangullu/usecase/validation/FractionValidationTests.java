package com.erhangullu.usecase.validation;

import com.erhangullu.usecase.configuration.HelperConfiguration;
import com.erhangullu.usecase.configuration.ServiceConfiguration;
import com.erhangullu.usecase.configuration.ValidationConfiguration;
import com.erhangullu.usecase.entity.Fraction;
import com.erhangullu.usecase.exception.FractionDataAlreadyExistsException;
import com.erhangullu.usecase.exception.InvalidFractionExeption;
import com.erhangullu.usecase.helper.TestDataProvider;
import com.erhangullu.usecase.repository.FractionRepository;
import com.erhangullu.usecase.repository.MeterReadingRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;


@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {ServiceConfiguration.class, ValidationConfiguration.class, HelperConfiguration.class}, loader = AnnotationConfigContextLoader.class)
public class FractionValidationTests {

    @Autowired
    TestDataProvider testDataProvider;

    @MockBean
    MeterReadingRepository meterReadingRepository;
    @MockBean
    FractionRepository fractionRepository;

    @Autowired
    FractionValidation fractionValidation;
    List<Fraction> testProfileFractions = new ArrayList<>();
    List<Fraction> invalidFractions = new ArrayList<>();
    List<Fraction> existingProfileFractions = new ArrayList<>();

    @Before
    public void prepareMockRepository() {
        testProfileFractions = testDataProvider.getValidFractionTestProfile1();
        existingProfileFractions = testDataProvider.getValidFractionTestProfile2();
        invalidFractions = testDataProvider.getInvalidFractionTestData_1();

        when(fractionRepository.findByProfile(eq(TestDataProvider.TEST_PROFILE_2))).thenReturn(existingProfileFractions);
        when(fractionRepository.findByProfile(eq(TestDataProvider.TEST_PROFILE_1))).thenReturn(new ArrayList<>());
    }

    @Test(expected = FractionDataAlreadyExistsException.class)
    public void validateFractionListShouldThrowExceptionWhenFractionsAlreadyExistsForProfile() {
        fractionValidation.validateFractionList(existingProfileFractions);
    }

    @Test(expected = InvalidFractionExeption.class)
    public void validateFractionListShouldThrowExceptionWhenFractionsSumInvalid() {
        fractionValidation.validateFractionList(invalidFractions);
    }

    @Test
    public void validateFractionListShouldSuccessWhenFractionsSumValid() {
        fractionValidation.validateFractionList(testProfileFractions);
    }
}