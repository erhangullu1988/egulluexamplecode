package com.erhangullu.usecase.configuration;

import com.erhangullu.usecase.helper.TestDataProvider;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;

@TestConfiguration
public class HelperConfiguration {

    @Bean
    public TestDataProvider testDataProvider() {
        return new TestDataProvider();
    }
}
