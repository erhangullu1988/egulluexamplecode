package com.erhangullu.usecase.controller;

import com.erhangullu.usecase.configuration.HelperConfiguration;
import com.erhangullu.usecase.entity.MeterReading;
import com.erhangullu.usecase.helper.TestDataProvider;
import com.erhangullu.usecase.repository.FractionRepository;
import com.erhangullu.usecase.repository.MeterReadingRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;
import java.nio.charset.Charset;
import java.time.Month;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = HelperConfiguration.class)
@AutoConfigureMockMvc
public class MeterReadingControllerTest {
    @Autowired
    TestDataProvider testDataProvider;

    @Autowired
    MeterReadingRepository meterReadingRepository;

    @Autowired
    FractionRepository fractionRepository;


    private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

    @Autowired
    private MockMvc mockMvc;

    @SuppressWarnings("rawtypes")
    private HttpMessageConverter mappingJackson2HttpMessageConverter;

    @Autowired
    void setConverters(HttpMessageConverter<?>[] converters) {

        this.mappingJackson2HttpMessageConverter = Arrays.asList(converters).stream()
                .filter(hmc -> hmc instanceof MappingJackson2HttpMessageConverter).findAny().get();

        assertNotNull("the JSON message converter must not be null", this.mappingJackson2HttpMessageConverter);
    }

    @Before
    public void setup() {
        fractionRepository.deleteAllInBatch();
        fractionRepository.flush();
        meterReadingRepository.deleteAllInBatch();
        meterReadingRepository.flush();
    }

    @Test
    public void shouldReturn201WhenAllFractionsCreated() throws Exception {
        fractionRepository.save(testDataProvider.getValidFractionTestProfile1());
        fractionRepository.flush();

        String url = "/meterreading";
        mockMvc.perform(put(url)
                .content(json(testDataProvider.getValidMeterReadingTestConnection1()))
                .contentType(contentType))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$[0].connection", is(TestDataProvider.TEST_CONNECTION_ID_1)));

        Assert.assertEquals(12L, meterReadingRepository.count());
    }

    @Test
    public void shouldReturn406WhenInvalidMeteringDataSend() throws Exception {
        fractionRepository.save(testDataProvider.getValidFractionTestProfile1());
        fractionRepository.flush();

        String url = "/meterreading";
        mockMvc.perform(put(url)
                .content(json(testDataProvider.getInvalidMeterReadingTestData()))
                .contentType(contentType))
                .andExpect(status().isNotAcceptable())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.message", is("Invalid meter data for following connections: " + TestDataProvider.TEST_CONNECTION_ID_3)));

        Assert.assertEquals(0L, meterReadingRepository.count());
    }

    @Test
    public void shouldReturn207WhenValidInvalidMeteringDataSend() throws Exception {
        fractionRepository.save(testDataProvider.getValidFractionTestProfile1());
        fractionRepository.flush();

        List<MeterReading> testData = new ArrayList<>();
        testData.addAll(testDataProvider.getInvalidMeterReadingTestData());
        testData.addAll(testDataProvider.getValidMeterReadingTestConnection1());

        String url = "/meterreading";
        mockMvc.perform(put(url)
                .content(json(testData))
                .contentType(contentType))
                .andExpect(status().isMultiStatus())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.message", is("Invalid meter data for following connections: " + TestDataProvider.TEST_CONNECTION_ID_3)));

        Assert.assertEquals(12L, meterReadingRepository.count());
    }

    @Test
    public void shouldReturn200WhenAllMeteringDataRequested_1() throws Exception {
        meterReadingRepository.save(testDataProvider.getValidMeterReadingTestConnection1());
        meterReadingRepository.save(testDataProvider.getValidMeterReadingTestConnection2());
        meterReadingRepository.flush();

        String url = "/meterreading";
        mockMvc.perform(get(url))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(24)));

        Assert.assertEquals(24L, meterReadingRepository.count());
    }

    @Test
    public void shouldReturn200WhenAllMeteringDataRequested_2() throws Exception {

        String url = "/meterreading";
        mockMvc.perform(get(url))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(0)));

        Assert.assertEquals(0L, meterReadingRepository.count());
    }

    @Test
    public void shouldReturn200WhenMeteringDataRequestedWithConnection() throws Exception {
        meterReadingRepository.save(testDataProvider.getValidMeterReadingTestConnection1());
        meterReadingRepository.save(testDataProvider.getValidMeterReadingTestConnection2());
        meterReadingRepository.flush();

        String url = String.format("/meterreading/%d", TestDataProvider.TEST_CONNECTION_ID_1);
        mockMvc.perform(get(url))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(12)))
                .andExpect(jsonPath("$[0].connection", is(TestDataProvider.TEST_CONNECTION_ID_1)));

        Assert.assertEquals(24L, meterReadingRepository.count());
    }

    @Test
    public void shouldReturn404WhenInvalidMeteringDataRequestedWithConnection() throws Exception {
        meterReadingRepository.save(testDataProvider.getValidMeterReadingTestConnection1());
        meterReadingRepository.save(testDataProvider.getValidMeterReadingTestConnection2());
        meterReadingRepository.flush();

        String url = String.format("/meterreading/%d", TestDataProvider.INVALID_CONNECTION_1);
        mockMvc.perform(get(url))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.message", is("Connection not found: " + TestDataProvider.INVALID_CONNECTION_1)));

        Assert.assertEquals(24L, meterReadingRepository.count());
    }

    @Test
    public void shouldReturn200WhenMeteringDataRequestedWithMonthAndConnection() throws Exception {
        meterReadingRepository.save(testDataProvider.getValidMeterReadingTestConnection1());
        meterReadingRepository.save(testDataProvider.getValidMeterReadingTestConnection2());
        meterReadingRepository.flush();

        String url = String.format("/meterreading/%d/%s", TestDataProvider.TEST_CONNECTION_ID_1, Month.APRIL.toString());
        mockMvc.perform(get(url))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.value", is(TestDataProvider.READING_TEST_VALUE_APRIL.intValue())))
                .andExpect(jsonPath("$.month", is(Month.APRIL.toString())))
                .andExpect(jsonPath("$.connection", is(TestDataProvider.TEST_CONNECTION_ID_1)));

        Assert.assertEquals(24L, meterReadingRepository.count());
    }

    @Test
    public void shouldReturn404WhenInvalidMeteringDataRequestedWithMonthAndConnection() throws Exception {
        meterReadingRepository.save(testDataProvider.getValidMeterReadingTestConnection1());
        meterReadingRepository.save(testDataProvider.getValidMeterReadingTestConnection2());
        meterReadingRepository.flush();

        String url = String.format("/meterreading/%d/%s", TestDataProvider.INVALID_CONNECTION_1, Month.APRIL.toString());
        mockMvc.perform(get(url))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.message", is("Connection not found: " + TestDataProvider.INVALID_CONNECTION_1)));

        Assert.assertEquals(24L, meterReadingRepository.count());
    }

    @Test
    public void shouldReturn204WhenMeteringDataDeletedByConnection() throws Exception {
        meterReadingRepository.save(testDataProvider.getValidMeterReadingTestConnection1());
        meterReadingRepository.save(testDataProvider.getValidMeterReadingTestConnection2());
        meterReadingRepository.flush();

        Assert.assertEquals(24L, meterReadingRepository.count());

        String url = String.format("/meterreading/%d", TestDataProvider.TEST_CONNECTION_ID_1);
        mockMvc.perform(delete(url))
                .andExpect(status().isNoContent());

        Assert.assertEquals(12L, meterReadingRepository.count());
    }

    @Test
    public void shouldReturn404WhenInvalidMeteringDataDeleteRequestedByConnection() throws Exception {
        meterReadingRepository.save(testDataProvider.getValidMeterReadingTestConnection1());
        meterReadingRepository.flush();

        Assert.assertEquals(12L, meterReadingRepository.count());

        String url = String.format("/meterreading/%d", TestDataProvider.INVALID_CONNECTION_1);
        mockMvc.perform(delete(url))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.message", is("Connection not found: " + TestDataProvider.INVALID_CONNECTION_1)));

        Assert.assertEquals(12L, meterReadingRepository.count());
    }

    @SuppressWarnings("unchecked")
    protected String json(Object o) {
        MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
        try {
            this.mappingJackson2HttpMessageConverter.write(o, MediaType.APPLICATION_JSON_UTF8, mockHttpOutputMessage);
        } catch (HttpMessageNotWritableException | IOException e) {
            return null;
        }
        return mockHttpOutputMessage.getBodyAsString();
    }

}
