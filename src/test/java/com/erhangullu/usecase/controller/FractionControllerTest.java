package com.erhangullu.usecase.controller;

import com.erhangullu.usecase.configuration.HelperConfiguration;
import com.erhangullu.usecase.entity.Fraction;
import com.erhangullu.usecase.helper.TestDataProvider;
import com.erhangullu.usecase.repository.FractionRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = HelperConfiguration.class)
@AutoConfigureMockMvc
public class FractionControllerTest {
    @Autowired
    TestDataProvider testDataProvider;

    @Autowired
    FractionRepository fractionRepository;

    private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

    @Autowired
    private MockMvc mockMvc;

    @SuppressWarnings("rawtypes")
    private HttpMessageConverter mappingJackson2HttpMessageConverter;

    @Autowired
    void setConverters(HttpMessageConverter<?>[] converters) {

        this.mappingJackson2HttpMessageConverter = Arrays.asList(converters).stream()
                .filter(hmc -> hmc instanceof MappingJackson2HttpMessageConverter).findAny().get();

        assertNotNull("the JSON message converter must not be null", this.mappingJackson2HttpMessageConverter);
    }

    @Before
    public void setup() {
        fractionRepository.deleteAll();
        fractionRepository.flush();
    }

    @Test
    public void shouldReturn200WhenAllFractionsRequested_1() throws Exception {
        List<Fraction> fractionList = testDataProvider.getValidFractionTestProfile1();
        fractionRepository.save(fractionList);
        fractionRepository.flush();


        String url = "/fraction";
        mockMvc.perform(get(url))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(12)))
                .andExpect(jsonPath("$[0].profile", is(TestDataProvider.TEST_PROFILE_1)));
    }

    @Test
    public void shouldReturn200WhenAllFractionsRequested_2() throws Exception {
        String url = "/fraction";
        mockMvc.perform(get(url))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(0)));
    }

    @Test
    public void shouldReturn201WhenAllFractionsCreated() throws Exception {
        String url = "/fraction";
        mockMvc.perform(put(url)
                .content(json(testDataProvider.getValidFractionTestProfile1()))
                .contentType(contentType))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$[0].profile", is(TestDataProvider.TEST_PROFILE_1)));

        Assert.assertEquals(12L, fractionRepository.count());
    }

    @Test
    public void shouldReturn406WhenInvalidFractionSend() throws Exception {
        String url = "/fraction";
        String errorMessage = String.format("Fraction data is invalid for following profiles: %s", TestDataProvider.INVALID_PROFILE_1);
        mockMvc.perform(put(url)
                .content(json(testDataProvider.getInvalidFractionTestData_1()))
                .contentType(contentType))
                .andExpect(status().isNotAcceptable())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.message", is(errorMessage)));

        Assert.assertEquals(0L, fractionRepository.count());
    }

    @Test
    public void shouldReturn406WhenDuplicateFractionSend() throws Exception {
        fractionRepository.save(testDataProvider.getValidFractionTestProfile1());
        fractionRepository.flush();

        String url = "/fraction";
        String errorMessage = String.format("Fraction data already exists for profiles: %s", TestDataProvider.TEST_PROFILE_1);
        mockMvc.perform(put(url)
                .content(json(testDataProvider.getValidFractionTestProfile1()))
                .contentType(contentType))
                .andExpect(status().isNotAcceptable())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.message", is(errorMessage)));
    }

    @Test
    public void shouldReturn200WhenFractionRequestedForProfile() throws Exception {
        List<Fraction> fractionList = new ArrayList<>();
        fractionList.addAll(testDataProvider.getValidFractionTestProfile1());
        fractionList.addAll(testDataProvider.getValidFractionTestProfile2());
        fractionRepository.save(fractionList);
        fractionRepository.flush();

        String url = String.format("/fraction/%s", TestDataProvider.TEST_PROFILE_1);
        mockMvc.perform(get(url))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(12)));

        Assert.assertEquals(24L, fractionRepository.count());
    }

    @Test
    public void shouldReturn204WhenFractionDeletedForProfile() throws Exception {
        List<Fraction> fractionList = new ArrayList<>();
        fractionList.addAll(testDataProvider.getValidFractionTestProfile1());
        fractionList.addAll(testDataProvider.getValidFractionTestProfile2());
        fractionRepository.save(fractionList);
        fractionRepository.flush();

        Assert.assertEquals(24L, fractionRepository.count());

        String url = String.format("/fraction/%s", TestDataProvider.TEST_PROFILE_1);
        mockMvc.perform(delete(url))
                .andExpect(status().isNoContent());

        Assert.assertEquals(12L, fractionRepository.count());
    }

    @Test
    public void shouldReturn404WhenFractionRequestedForInvalidProfile() throws Exception {
        String url = String.format("/fraction/%s", TestDataProvider.INVALID_PROFILE_1);
        mockMvc.perform(get(url))
                .andExpect(status().isNotFound());
    }

    @Test
    public void shouldReturn404WhenFractionDeleteRequestedForInvalidProfile() throws Exception {
        String url = String.format("/fraction/%s", TestDataProvider.INVALID_PROFILE_1);
        mockMvc.perform(delete(url))
                .andExpect(status().isNotFound());

    }

    @SuppressWarnings("unchecked")
    protected String json(Object o) {
        MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
        try {
            this.mappingJackson2HttpMessageConverter.write(o, MediaType.APPLICATION_JSON_UTF8, mockHttpOutputMessage);
        } catch (HttpMessageNotWritableException | IOException e) {
            return null;
        }
        return mockHttpOutputMessage.getBodyAsString();
    }

}
