package com.erhangullu.usecase.controller;

import com.erhangullu.usecase.configuration.HelperConfiguration;
import com.erhangullu.usecase.entity.MeterReading;
import com.erhangullu.usecase.helper.TestDataProvider;
import com.erhangullu.usecase.repository.MeterReadingRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.time.Month;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = HelperConfiguration.class)
@AutoConfigureMockMvc
public class ConsumptionControllerTest {

    @Autowired
    TestDataProvider testDataProvider;

    @Autowired
    MeterReadingRepository meterReadingRepository;

    @Autowired
    private MockMvc mockMvc;

    @Before
    public void setup() throws Exception {
        this.meterReadingRepository.deleteAllInBatch();
    }

    public void prepareValidMeterReadingData() {
        List<MeterReading> meterReadings = testDataProvider.getValidMeterReadingTestConnection1();
        meterReadings.forEach(meterReading -> meterReadingRepository.save(meterReading));
        this.meterReadingRepository.flush();
    }

    @Test
    public void shouldReturn200ForSpesificMonthWhenDataExists() throws Exception {
        prepareValidMeterReadingData();

        String url = String.format("/consumption?month=%s&connection=%d", Month.APRIL.toString(), TestDataProvider.TEST_CONNECTION_ID_1);
        mockMvc.perform(get(url))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.connection", is(TestDataProvider.TEST_CONNECTION_ID_1)))
                .andExpect(jsonPath("$.month", is(Month.APRIL.toString())))
                .andExpect(jsonPath("$.value", is(9)));
    }

    @Test
    public void shouldReturn404ForSpesificMonthWhenDataDoesNotExists() throws Exception {

        String url = String.format("/consumption?month=%s&connection=%d", Month.APRIL.toString(), TestDataProvider.INVALID_CONNECTION_1);
        mockMvc.perform(get(url))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.message", is("Meter data does not exists for following connection: " + TestDataProvider.INVALID_CONNECTION_1)));
    }

    @Test
    public void shouldReturn200ForConnectionWhenDataExists() throws Exception {
        prepareValidMeterReadingData();

        String url = String.format("/consumption/connection/%d", TestDataProvider.TEST_CONNECTION_ID_1);
        mockMvc.perform(get(url))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(12)))
                .andExpect(jsonPath("$[0].connection", is(TestDataProvider.TEST_CONNECTION_ID_1)));
    }

    @Test
    public void shouldReturn404ForConnectionWhenDataDoesNotExists() throws Exception {
        String url = String.format("/consumption/connection/%d", TestDataProvider.INVALID_CONNECTION_1);
        mockMvc.perform(get(url))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.message", is("Meter data does not exists for following connection: " + TestDataProvider.INVALID_CONNECTION_1)));
    }


}
