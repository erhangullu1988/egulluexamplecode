package com.erhangullu.usecase.service;


import com.erhangullu.usecase.configuration.HelperConfiguration;
import com.erhangullu.usecase.configuration.ServiceConfiguration;
import com.erhangullu.usecase.entity.MeterReading;
import com.erhangullu.usecase.exception.NotFoundException;
import com.erhangullu.usecase.helper.TestDataProvider;
import com.erhangullu.usecase.repository.FractionRepository;
import com.erhangullu.usecase.repository.MeterReadingRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.time.Month;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {ServiceConfiguration.class, HelperConfiguration.class}, loader = AnnotationConfigContextLoader.class)
public class MeterReadingServiceTests {

    @Autowired
    TestDataProvider testDataProvider;

    @MockBean
    MeterReadingRepository meterReadingRepository;
    @MockBean
    FractionRepository fractionRepository;
    @Autowired
    MeterReadingService meterReadingService;

    List<MeterReading> testConnectionAllReadings = new ArrayList<>();
    List<MeterReading> testConnection1Readings;
    List<MeterReading> testConnection2Readings;

    @Before
    public void prepareMockRepository() {
        testConnection1Readings = testDataProvider.getValidMeterReadingTestConnection1();
        testConnection2Readings = testDataProvider.getValidMeterReadingTestConnection2();

        testConnectionAllReadings.addAll(testConnection1Readings);
        testConnectionAllReadings.addAll(testConnection2Readings);

        testConnection1Readings.forEach(meterReading -> when(meterReadingRepository.findByConnectionAndMonth(eq(meterReading.getConnection()), eq(meterReading.getMonth().toString()))).thenReturn(meterReading));
        when(meterReadingRepository.findByConnectionAndMonth(any(), eq(TestDataProvider.INVALID_PROFILE_1))).thenReturn(null);
        when(meterReadingRepository.findByConnection(eq(TestDataProvider.TEST_CONNECTION_ID_1))).thenReturn(testConnection1Readings);
        when(meterReadingRepository.findByConnection(eq(TestDataProvider.TEST_CONNECTION_ID_2))).thenReturn(testConnection2Readings);
        when(meterReadingRepository.findAll()).thenReturn(testConnectionAllReadings);
        when(meterReadingRepository.save(eq(testConnection1Readings))).thenReturn(testConnection1Readings);
        when(meterReadingRepository.deleteByConnection(eq(TestDataProvider.TEST_CONNECTION_ID_1))).thenReturn(12);
        when(meterReadingRepository.deleteByConnection(eq(TestDataProvider.INVALID_CONNECTION_1))).thenReturn(0);
        doNothing().when(meterReadingRepository).flush();
    }

    @Test
    public void addAllTest() {
        meterReadingService.addAll(testConnection1Readings);
    }

    @Test(expected = NotFoundException.class)
    public void findByInvalidConnectionTest() {
        meterReadingService.findByConnection(TestDataProvider.INVALID_CONNECTION_1);
    }

    @Test(expected = NotFoundException.class)
    public void findByMonthAndInvalidProfileTest() {
        meterReadingService.findByConnectionAndMonth(TestDataProvider.INVALID_CONNECTION_1, Month.APRIL);
    }

    @Test(expected = NotFoundException.class)
    public void deleteByInvalidConnectionTest() {
        meterReadingService.deleteByConnection(TestDataProvider.INVALID_CONNECTION_1);
    }

    @Test
    public void findByConnection1Test() {
        List<MeterReading> meterReadings = meterReadingService.findByConnection(TestDataProvider.TEST_CONNECTION_ID_1);
        assertThat(meterReadings).size().isEqualTo(12);
        assertThat(meterReadings.get(0).getProfile()).isEqualTo(TestDataProvider.TEST_PROFILE_1);
    }

    @Test
    public void findByConnection2Test() {
        List<MeterReading> meterReadings = meterReadingService.findByConnection(TestDataProvider.TEST_CONNECTION_ID_2);
        assertThat(meterReadings).size().isEqualTo(12);
        assertThat(meterReadings.get(0).getProfile()).isEqualTo(TestDataProvider.TEST_PROFILE_2);
    }

    @Test
    public void findByConnectionAndMonthTest() {
        MeterReading meterReading = meterReadingService.findByConnectionAndMonth(TestDataProvider.TEST_CONNECTION_ID_1, Month.APRIL);

        assertThat(meterReading.getProfile()).isEqualTo(TestDataProvider.TEST_PROFILE_1);
        assertThat(meterReading.getValue()).isEqualTo(TestDataProvider.READING_TEST_VALUE_APRIL);
        assertThat(meterReading.getMonth()).isEqualTo(Month.APRIL);
    }


    @Test
    public void findAllTest() {
        List<MeterReading> meterReadings = meterReadingService.findAll();
        assertThat(meterReadings).size().isEqualTo(24);
    }

    @Test
    public void deleteByConnectionTest() {
        meterReadingService.deleteByConnection(TestDataProvider.TEST_CONNECTION_ID_1);
    }
}

