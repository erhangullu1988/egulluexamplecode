package com.erhangullu.usecase.service;

import com.erhangullu.usecase.configuration.HelperConfiguration;
import com.erhangullu.usecase.configuration.ServiceConfiguration;
import com.erhangullu.usecase.entity.Fraction;
import com.erhangullu.usecase.exception.NotFoundException;
import com.erhangullu.usecase.helper.TestDataProvider;
import com.erhangullu.usecase.repository.FractionRepository;
import com.erhangullu.usecase.repository.MeterReadingRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {ServiceConfiguration.class, HelperConfiguration.class}, loader = AnnotationConfigContextLoader.class)
public class FractionServiceTests {

    @Autowired
    TestDataProvider testDataProvider;

    @MockBean
    MeterReadingRepository meterReadingRepository;
    @MockBean
    FractionRepository fractionRepository;

    @Autowired
    FractionService fractionService;
    private List<Fraction> profile1Fractions;
    private List<Fraction> profile2Fractions;

    @Before
    public void prepareMockRepository() {

        profile1Fractions = testDataProvider.getValidFractionTestProfile1();
        profile2Fractions = testDataProvider.getValidFractionTestProfile2();

        List<Fraction> allFractions = new ArrayList<>();
        allFractions.addAll(profile1Fractions);
        allFractions.addAll(profile2Fractions);

        when(fractionRepository.findAll()).thenReturn(allFractions);
        when(fractionRepository.findByProfile(eq(TestDataProvider.TEST_PROFILE_1))).thenReturn(profile1Fractions);
        when(fractionRepository.findByProfile(eq(TestDataProvider.TEST_PROFILE_2))).thenReturn(profile2Fractions);
        when(fractionRepository.findByProfile(eq(TestDataProvider.INVALID_PROFILE_1))).thenReturn(new ArrayList<Fraction>());
        when(fractionRepository.save(eq(profile1Fractions))).thenReturn(profile1Fractions);
        when(fractionRepository.save(eq(profile2Fractions))).thenReturn(profile2Fractions);
        when(fractionRepository.deleteByProfile(eq(TestDataProvider.TEST_PROFILE_1))).thenReturn(12);
        when(fractionRepository.deleteByProfile(eq(TestDataProvider.INVALID_PROFILE_1))).thenReturn(0);
        doNothing().when(fractionRepository).flush();

    }

    @Test
    public void findAllTest() {
        List<Fraction> fractionList = fractionService.findAll();
        assertThat(fractionList).size().isEqualTo(24);
    }

    @Test
    public void findByProfileTest() {
        List<Fraction> fractionList = fractionService.findByProfile(TestDataProvider.TEST_PROFILE_1);
        assertThat(fractionList).size().isEqualTo(12);
        assertThat(fractionList.get(0).getProfile()).isEqualTo(TestDataProvider.TEST_PROFILE_1);
    }

    @Test(expected = NotFoundException.class)
    public void findByInvalidProfileTest() {
        fractionService.findByProfile(TestDataProvider.INVALID_PROFILE_1);
    }


    @Test
    public void addAllTest() {
        fractionService.addAll(profile1Fractions);
    }

    @Test
    public void deleteByProfileTest() {
        fractionService.deleteByProfile(TestDataProvider.TEST_PROFILE_1);
    }

    @Test(expected = NotFoundException.class)
    public void deleteByInvalidProfileTest() {
        fractionService.deleteByProfile(TestDataProvider.INVALID_PROFILE_1);
    }
}
