package com.erhangullu.usecase.service;


import com.erhangullu.usecase.configuration.HelperConfiguration;
import com.erhangullu.usecase.configuration.ServiceConfiguration;
import com.erhangullu.usecase.entity.Consumption;
import com.erhangullu.usecase.entity.MeterReading;
import com.erhangullu.usecase.exception.MeterDataNotExistsException;
import com.erhangullu.usecase.helper.TestDataProvider;
import com.erhangullu.usecase.repository.FractionRepository;
import com.erhangullu.usecase.repository.MeterReadingRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.time.Month;
import java.util.List;

import static java.util.stream.Collectors.toMap;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;


@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {ServiceConfiguration.class, HelperConfiguration.class}, loader = AnnotationConfigContextLoader.class)
public class ConsumptionServiceTests {


    @Autowired
    TestDataProvider testDataProvider;

    @MockBean
    MeterReadingRepository meterReadingRepository;
    @MockBean
    FractionRepository fractionRepository;
    @Autowired
    ConsumptionService consumptionService;

    @Before
    public void prepareMockRepository() {

        List<MeterReading> meterReadings = testDataProvider.getValidMeterReadingTestConnection1();
        meterReadings.stream()
                .collect(toMap(MeterReading::getMonth, meterReading -> meterReading))
                .forEach((month, meterReading) -> {
                    when(meterReadingRepository.findByConnectionAndMonth(eq(TestDataProvider.TEST_CONNECTION_ID_1), eq(month.toString()))).thenReturn(meterReading);
                });

        when(meterReadingRepository.findByConnectionAndMonth(eq(TestDataProvider.INVALID_CONNECTION_1), any())).thenReturn(null);
        when(meterReadingRepository.findByConnection(eq(TestDataProvider.TEST_CONNECTION_ID_1))).thenReturn(meterReadings);
    }

    @Test(expected = MeterDataNotExistsException.class)
    public void throwExceptionWhenProfileNotFound() {
        consumptionService.find(Month.FEBRUARY, TestDataProvider.INVALID_CONNECTION_1);
    }

    @Test
    public void returnFirstMonthReadingAsConsumption() {
        Consumption consumption = consumptionService.find(Month.JANUARY, TestDataProvider.TEST_CONNECTION_ID_1);

        assertThat(consumption.getConnection()).isEqualTo(TestDataProvider.TEST_CONNECTION_ID_1);
        assertThat(consumption.getMonth()).isEqualTo(Month.JANUARY);
        assertThat(consumption.getValue()).isEqualTo(TestDataProvider.READING_TEST_VALUE_JANUARY);
    }

    @Test
    public void returnDifferenceInReadingAsConsumptionForMonthAndConnection_1() {
        Consumption consumption = consumptionService.find(Month.FEBRUARY, TestDataProvider.TEST_CONNECTION_ID_1);

        assertThat(consumption.getConnection()).isEqualTo(TestDataProvider.TEST_CONNECTION_ID_1);
        assertThat(consumption.getMonth()).isEqualTo(Month.FEBRUARY);
        assertThat(consumption.getValue()).isEqualTo(TestDataProvider.READING_TEST_VALUE_FEBRUARY - TestDataProvider.READING_TEST_VALUE_JANUARY);
    }

    @Test
    public void returnDifferenceInReadingAsConsumptionForMonthAndConnection_2() {
        Consumption consumption = consumptionService.find(Month.AUGUST, TestDataProvider.TEST_CONNECTION_ID_1);

        assertThat(consumption.getConnection()).isEqualTo(TestDataProvider.TEST_CONNECTION_ID_1);
        assertThat(consumption.getMonth()).isEqualTo(Month.AUGUST);
        assertThat(consumption.getValue()).isEqualTo(TestDataProvider.READING_TEST_VALUE_AUGUST - TestDataProvider.READING_TEST_VALUE_JULY);
    }

    @Test
    public void returnDifferenceInReadingAsConsumptionForConnection() {
        List<Consumption> consumptionList = consumptionService.findByConnection(TestDataProvider.TEST_CONNECTION_ID_1);
        assertThat(consumptionList).size().isEqualTo(12);
    }
}
