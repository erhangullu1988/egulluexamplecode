package com.erhangullu.usecase.helper;


import com.erhangullu.usecase.entity.Fraction;
import com.erhangullu.usecase.entity.MeterReading;
import lombok.Getter;

import java.time.Month;
import java.util.ArrayList;
import java.util.List;

@Getter
public class TestDataProvider {

    public static final String INVALID_PROFILE_1 = "invalid_profile_1";
    public static final String INVALID_PROFILE_2 = "invalid_profile_2";
    public static final String TEST_PROFILE_1 = "test_profile_1";
    public static final String TEST_PROFILE_2 = "test_profile_2";
    public static final Integer TEST_CONNECTION_ID_1 = 1;
    public static final Integer TEST_CONNECTION_ID_2 = 2;
    public static final Integer TEST_CONNECTION_ID_3 = 3;
    public static final Integer INVALID_CONNECTION_1 = 99;
    public static final Integer INVALID_CONNECTION_2 = 100;
    public static final Long READING_TEST_VALUE_JANUARY = 18l;
    public static final Long READING_TEST_VALUE_FEBRUARY = 39l;
    public static final Long READING_TEST_VALUE_MARCH = 55l;
    public static final Long READING_TEST_VALUE_APRIL = 64l;
    public static final Long READING_TEST_VALUE_MAY = 74l;
    public static final Long READING_TEST_VALUE_JUNE = 74l;
    public static final Long READING_TEST_VALUE_JULY = 74l;
    public static final Long READING_TEST_VALUE_AUGUST = 77l;
    public static final Long READING_TEST_VALUE_SEPTEMBER = 81l;
    public static final Long READING_TEST_VALUE_OCTOBER = 92l;
    public static final Long READING_TEST_VALUE_NOVEMBER = 104l;
    public static final Long READING_TEST_VALUE_DECEMBER = 122l;
    private static final Double FRACTION_TEST_VALUE_JANUARY = 0.15d;
    private static final Double FRACTION_TEST_VALUE_FEBRUARY = 0.17d;
    private static final Double FRACTION_TEST_VALUE_MARCH = 0.13d;
    private static final Double FRACTION_TEST_VALUE_APRIL = 0.08d;
    private static final Double FRACTION_TEST_VALUE_MAY = 0.08d;
    private static final Double FRACTION_TEST_VALUE_JUNE = 0d;
    private static final Double FRACTION_TEST_VALUE_JULY = 0d;
    private static final Double FRACTION_TEST_VALUE_AUGUST = 0.02d;
    private static final Double FRACTION_TEST_VALUE_SEPTEMBER = 0.03d;
    private static final Double FRACTION_TEST_VALUE_OCTOBER = 0.09d;
    private static final Double FRACTION_TEST_VALUE_NOVEMBER = 0.10d;
    private static final Double FRACTION_TEST_VALUE_DECEMBER = 0.15d;
    private static final Double FRACTION_INVALID_TEST_VALUE = 1d;
    List<MeterReading> validMeterReadingTestConnection1 = new ArrayList<>();
    List<MeterReading> validMeterReadingTestConnection2 = new ArrayList<>();
    List<MeterReading> invalidMeterReadingTestData = new ArrayList<>();
    List<Fraction> validFractionTestProfile1 = new ArrayList<>();
    List<Fraction> validFractionTestProfile2 = new ArrayList<>();
    List<Fraction> invalidFractionTestData_1 = new ArrayList<>();

    public TestDataProvider() {
        initData();
    }

    private void initData() {

        validFractionTestProfile1.add(new Fraction(1, Month.JANUARY, TEST_PROFILE_1, FRACTION_TEST_VALUE_JANUARY));
        validFractionTestProfile1.add(new Fraction(2, Month.FEBRUARY, TEST_PROFILE_1, FRACTION_TEST_VALUE_FEBRUARY));
        validFractionTestProfile1.add(new Fraction(3, Month.MARCH, TEST_PROFILE_1, FRACTION_TEST_VALUE_MARCH));
        validFractionTestProfile1.add(new Fraction(4, Month.APRIL, TEST_PROFILE_1, FRACTION_TEST_VALUE_APRIL));
        validFractionTestProfile1.add(new Fraction(5, Month.MAY, TEST_PROFILE_1, FRACTION_TEST_VALUE_MAY));
        validFractionTestProfile1.add(new Fraction(6, Month.JUNE, TEST_PROFILE_1, FRACTION_TEST_VALUE_JUNE));
        validFractionTestProfile1.add(new Fraction(7, Month.JULY, TEST_PROFILE_1, FRACTION_TEST_VALUE_JULY));
        validFractionTestProfile1.add(new Fraction(8, Month.AUGUST, TEST_PROFILE_1, FRACTION_TEST_VALUE_AUGUST));
        validFractionTestProfile1.add(new Fraction(9, Month.SEPTEMBER, TEST_PROFILE_1, FRACTION_TEST_VALUE_SEPTEMBER));
        validFractionTestProfile1.add(new Fraction(10, Month.OCTOBER, TEST_PROFILE_1, FRACTION_TEST_VALUE_OCTOBER));
        validFractionTestProfile1.add(new Fraction(11, Month.NOVEMBER, TEST_PROFILE_1, FRACTION_TEST_VALUE_NOVEMBER));
        validFractionTestProfile1.add(new Fraction(12, Month.DECEMBER, TEST_PROFILE_1, FRACTION_TEST_VALUE_DECEMBER));
        validFractionTestProfile2.add(new Fraction(13, Month.JANUARY, TEST_PROFILE_2, FRACTION_TEST_VALUE_JANUARY));
        validFractionTestProfile2.add(new Fraction(14, Month.FEBRUARY, TEST_PROFILE_2, FRACTION_TEST_VALUE_FEBRUARY));
        validFractionTestProfile2.add(new Fraction(15, Month.MARCH, TEST_PROFILE_2, FRACTION_TEST_VALUE_MARCH));
        validFractionTestProfile2.add(new Fraction(16, Month.APRIL, TEST_PROFILE_2, FRACTION_TEST_VALUE_APRIL));
        validFractionTestProfile2.add(new Fraction(17, Month.MAY, TEST_PROFILE_2, FRACTION_TEST_VALUE_MAY));
        validFractionTestProfile2.add(new Fraction(18, Month.JUNE, TEST_PROFILE_2, FRACTION_TEST_VALUE_JUNE));
        validFractionTestProfile2.add(new Fraction(19, Month.JULY, TEST_PROFILE_2, FRACTION_TEST_VALUE_JULY));
        validFractionTestProfile2.add(new Fraction(20, Month.AUGUST, TEST_PROFILE_2, FRACTION_TEST_VALUE_AUGUST));
        validFractionTestProfile2.add(new Fraction(21, Month.SEPTEMBER, TEST_PROFILE_2, FRACTION_TEST_VALUE_SEPTEMBER));
        validFractionTestProfile2.add(new Fraction(22, Month.OCTOBER, TEST_PROFILE_2, FRACTION_TEST_VALUE_OCTOBER));
        validFractionTestProfile2.add(new Fraction(23, Month.NOVEMBER, TEST_PROFILE_2, FRACTION_TEST_VALUE_NOVEMBER));
        validFractionTestProfile2.add(new Fraction(24, Month.DECEMBER, TEST_PROFILE_2, FRACTION_TEST_VALUE_DECEMBER));
        invalidFractionTestData_1.add(new Fraction(13, Month.JANUARY, INVALID_PROFILE_1, FRACTION_TEST_VALUE_JANUARY));
        invalidFractionTestData_1.add(new Fraction(14, Month.FEBRUARY, INVALID_PROFILE_1, FRACTION_INVALID_TEST_VALUE));
        invalidFractionTestData_1.add(new Fraction(15, Month.MARCH, INVALID_PROFILE_1, FRACTION_TEST_VALUE_MARCH));
        invalidFractionTestData_1.add(new Fraction(16, Month.APRIL, INVALID_PROFILE_1, FRACTION_TEST_VALUE_APRIL));
        invalidFractionTestData_1.add(new Fraction(17, Month.MAY, INVALID_PROFILE_1, FRACTION_TEST_VALUE_MAY));
        invalidFractionTestData_1.add(new Fraction(18, Month.JUNE, INVALID_PROFILE_1, FRACTION_TEST_VALUE_JUNE));
        invalidFractionTestData_1.add(new Fraction(19, Month.JULY, INVALID_PROFILE_1, FRACTION_TEST_VALUE_JULY));
        invalidFractionTestData_1.add(new Fraction(20, Month.AUGUST, INVALID_PROFILE_1, FRACTION_INVALID_TEST_VALUE));
        invalidFractionTestData_1.add(new Fraction(21, Month.SEPTEMBER, INVALID_PROFILE_1, FRACTION_TEST_VALUE_SEPTEMBER));
        invalidFractionTestData_1.add(new Fraction(22, Month.OCTOBER, INVALID_PROFILE_1, FRACTION_TEST_VALUE_OCTOBER));
        invalidFractionTestData_1.add(new Fraction(23, Month.NOVEMBER, INVALID_PROFILE_1, FRACTION_TEST_VALUE_NOVEMBER));
        invalidFractionTestData_1.add(new Fraction(24, Month.DECEMBER, INVALID_PROFILE_1, FRACTION_TEST_VALUE_DECEMBER));


        validMeterReadingTestConnection1.add(new MeterReading(1, TEST_CONNECTION_ID_1, TEST_PROFILE_1, Month.JANUARY, READING_TEST_VALUE_JANUARY));
        validMeterReadingTestConnection1.add(new MeterReading(2, TEST_CONNECTION_ID_1, TEST_PROFILE_1, Month.FEBRUARY, READING_TEST_VALUE_FEBRUARY));
        validMeterReadingTestConnection1.add(new MeterReading(3, TEST_CONNECTION_ID_1, TEST_PROFILE_1, Month.MARCH, READING_TEST_VALUE_MARCH));
        validMeterReadingTestConnection1.add(new MeterReading(4, TEST_CONNECTION_ID_1, TEST_PROFILE_1, Month.APRIL, READING_TEST_VALUE_APRIL));
        validMeterReadingTestConnection1.add(new MeterReading(5, TEST_CONNECTION_ID_1, TEST_PROFILE_1, Month.MAY, READING_TEST_VALUE_MAY));
        validMeterReadingTestConnection1.add(new MeterReading(6, TEST_CONNECTION_ID_1, TEST_PROFILE_1, Month.JUNE, READING_TEST_VALUE_JUNE));
        validMeterReadingTestConnection1.add(new MeterReading(7, TEST_CONNECTION_ID_1, TEST_PROFILE_1, Month.JULY, READING_TEST_VALUE_JULY));
        validMeterReadingTestConnection1.add(new MeterReading(8, TEST_CONNECTION_ID_1, TEST_PROFILE_1, Month.AUGUST, READING_TEST_VALUE_AUGUST));
        validMeterReadingTestConnection1.add(new MeterReading(9, TEST_CONNECTION_ID_1, TEST_PROFILE_1, Month.SEPTEMBER, READING_TEST_VALUE_SEPTEMBER));
        validMeterReadingTestConnection1.add(new MeterReading(10, TEST_CONNECTION_ID_1, TEST_PROFILE_1, Month.OCTOBER, READING_TEST_VALUE_OCTOBER));
        validMeterReadingTestConnection1.add(new MeterReading(11, TEST_CONNECTION_ID_1, TEST_PROFILE_1, Month.NOVEMBER, READING_TEST_VALUE_NOVEMBER));
        validMeterReadingTestConnection1.add(new MeterReading(12, TEST_CONNECTION_ID_1, TEST_PROFILE_1, Month.DECEMBER, READING_TEST_VALUE_DECEMBER));
        validMeterReadingTestConnection2.add(new MeterReading(13, TEST_CONNECTION_ID_2, TEST_PROFILE_2, Month.JANUARY, READING_TEST_VALUE_JANUARY));
        validMeterReadingTestConnection2.add(new MeterReading(14, TEST_CONNECTION_ID_2, TEST_PROFILE_2, Month.FEBRUARY, READING_TEST_VALUE_FEBRUARY));
        validMeterReadingTestConnection2.add(new MeterReading(15, TEST_CONNECTION_ID_2, TEST_PROFILE_2, Month.MARCH, READING_TEST_VALUE_MARCH));
        validMeterReadingTestConnection2.add(new MeterReading(16, TEST_CONNECTION_ID_2, TEST_PROFILE_2, Month.APRIL, READING_TEST_VALUE_APRIL));
        validMeterReadingTestConnection2.add(new MeterReading(17, TEST_CONNECTION_ID_2, TEST_PROFILE_2, Month.MAY, READING_TEST_VALUE_MAY));
        validMeterReadingTestConnection2.add(new MeterReading(18, TEST_CONNECTION_ID_2, TEST_PROFILE_2, Month.JUNE, READING_TEST_VALUE_JUNE));
        validMeterReadingTestConnection2.add(new MeterReading(19, TEST_CONNECTION_ID_2, TEST_PROFILE_2, Month.JULY, READING_TEST_VALUE_JULY));
        validMeterReadingTestConnection2.add(new MeterReading(20, TEST_CONNECTION_ID_2, TEST_PROFILE_2, Month.AUGUST, READING_TEST_VALUE_AUGUST));
        validMeterReadingTestConnection2.add(new MeterReading(21, TEST_CONNECTION_ID_2, TEST_PROFILE_2, Month.SEPTEMBER, READING_TEST_VALUE_SEPTEMBER));
        validMeterReadingTestConnection2.add(new MeterReading(22, TEST_CONNECTION_ID_2, TEST_PROFILE_2, Month.OCTOBER, READING_TEST_VALUE_OCTOBER));
        validMeterReadingTestConnection2.add(new MeterReading(23, TEST_CONNECTION_ID_2, TEST_PROFILE_2, Month.NOVEMBER, READING_TEST_VALUE_NOVEMBER));
        validMeterReadingTestConnection2.add(new MeterReading(24, TEST_CONNECTION_ID_2, TEST_PROFILE_2, Month.DECEMBER, READING_TEST_VALUE_DECEMBER));
        invalidMeterReadingTestData.add(new MeterReading(25, TEST_CONNECTION_ID_3, INVALID_PROFILE_1, Month.JANUARY, READING_TEST_VALUE_JANUARY));
        invalidMeterReadingTestData.add(new MeterReading(26, TEST_CONNECTION_ID_3, INVALID_PROFILE_1, Month.FEBRUARY, READING_TEST_VALUE_FEBRUARY));
        invalidMeterReadingTestData.add(new MeterReading(27, TEST_CONNECTION_ID_3, INVALID_PROFILE_1, Month.MARCH, READING_TEST_VALUE_APRIL));
        invalidMeterReadingTestData.add(new MeterReading(28, TEST_CONNECTION_ID_3, INVALID_PROFILE_1, Month.APRIL, READING_TEST_VALUE_MARCH));
        invalidMeterReadingTestData.add(new MeterReading(29, TEST_CONNECTION_ID_3, INVALID_PROFILE_1, Month.MAY, READING_TEST_VALUE_MAY));
        invalidMeterReadingTestData.add(new MeterReading(30, TEST_CONNECTION_ID_3, INVALID_PROFILE_1, Month.JUNE, READING_TEST_VALUE_JUNE));
        invalidMeterReadingTestData.add(new MeterReading(31, TEST_CONNECTION_ID_3, INVALID_PROFILE_1, Month.JULY, READING_TEST_VALUE_JULY));
        invalidMeterReadingTestData.add(new MeterReading(32, TEST_CONNECTION_ID_3, INVALID_PROFILE_1, Month.AUGUST, READING_TEST_VALUE_AUGUST));
        invalidMeterReadingTestData.add(new MeterReading(33, TEST_CONNECTION_ID_3, INVALID_PROFILE_1, Month.SEPTEMBER, READING_TEST_VALUE_SEPTEMBER));
        invalidMeterReadingTestData.add(new MeterReading(34, TEST_CONNECTION_ID_3, INVALID_PROFILE_1, Month.OCTOBER, READING_TEST_VALUE_OCTOBER));
        invalidMeterReadingTestData.add(new MeterReading(35, TEST_CONNECTION_ID_3, INVALID_PROFILE_1, Month.NOVEMBER, READING_TEST_VALUE_NOVEMBER));
        invalidMeterReadingTestData.add(new MeterReading(36, TEST_CONNECTION_ID_3, INVALID_PROFILE_1, Month.DECEMBER, READING_TEST_VALUE_DECEMBER));
    }


}
