### How do I get set up? ###  
In the root directory of project  
Run tests  
- mvnw clean test  
Run applicaton:  
With Maven  
- mvnw spring-boot:run  
Jar:  
- mvnw clean package  
- java -jar usecase-1.0.0-SNAPSHOT.jar  
  
To access api documentation:  
After running application in localhost  
  
http://localhost:8080/swagger-ui.html  

